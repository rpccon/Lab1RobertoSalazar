package com.example.robertosalazar.robertosalazarlab;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    EditText perfil_p;
    private static final int REQUEST_CODE = 1234;
    TextView foto;
    Dialog matchTextDialog;
    ListView textListView;
    ArrayList<String> matchesText;
    ArrayList<Usuario> listaUsers = new ArrayList<Usuario>();
    ListView lv_users;
    Uri uriImg;
    private static final int VIDEO_CAPTURE = 101;
    RadioButton rb_male;
    RadioButton rb_female;
    EditText ma_name;

    Button aceptar;
    Button btn_tfoto;
    Button btn_voz;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        perfil_p = (EditText)findViewById(R.id.perfil_p);
        rb_male = (RadioButton)findViewById(R.id.rb_male);
        rb_female = (RadioButton)findViewById(R.id.rb_female);

        foto = (EditText)findViewById(R.id.foto);
        lv_users = (ListView)findViewById(R.id.lv_users);
        aceptar = (Button)findViewById(R.id.btn_acept);
        ma_name = (EditText)findViewById(R.id.ma_name);
        btn_tfoto = (Button)findViewById(R.id.btn_tfoto);
        btn_voz = (Button)findViewById(R.id.btn_voz);

        perfil_p.setEnabled(false);
        foto.setEnabled(false);

        btn_voz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                    intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "es-ES");
                    startActivityForResult(intent, REQUEST_CODE);


                } else {
                    Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rb_female.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    rb_male.setChecked(false);

            }
        });
        rb_male.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rb_female.setChecked(false);
            }
        });
        btn_tfoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent, VIDEO_CAPTURE);

            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ma_name.getText().toString().equals("") || perfil_p.getText().toString().equals("") || foto.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "No se han llenado todos los campos", Toast.LENGTH_SHORT).show();
                } else {


                    Usuario user = new Usuario();
                    user.setNombre(ma_name.getText().toString());
                    user.setPerfil(perfil_p.getText().toString());
                    String gen = "Female";
                    if (rb_male.isChecked()) {
                        gen = "Male";
                    }
                    user.setSexo(gen);

                    user.setFoto(foto.getText().toString());

                    listaUsers.add(user);

                    lv_users.setAdapter(new viewAdapter(MainActivity.this));
                }

            }
        });




    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( requestCode == VIDEO_CAPTURE ) {
            Uri videoUri = data . getData () ;

            if ( resultCode == RESULT_OK ) {
                uriImg=videoUri;
                foto.setText(uriImg.toString());
                Toast.makeText(MainActivity.this, uriImg.toString(), Toast.LENGTH_SHORT).show();
            } else if ( resultCode == RESULT_CANCELED ) {
                Toast . makeText (this , " Video recording cancelled .",
                        Toast . LENGTH_LONG ) . show () ;
            } else {
                Toast . makeText (this , " Failed to record video ",
                        Toast . LENGTH_LONG ) . show () ;
            }
        }
        else{
            matchTextDialog = new Dialog(MainActivity.this); //Create a Dialog
            matchTextDialog.setContentView(R.layout.dialog_matches_frag); //Link the new Dialog with the dialog_matches frag
            matchTextDialog.setTitle("Select Matching Text"); //Add title to the Dialog
            textListView = (ListView) matchTextDialog.findViewById(R.id.listView1);
            matchesText = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS); //Get data of data
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, matchesText );
            textListView.setAdapter(adapter);
            textListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    perfil_p.setText(matchesText.get(i).toString());


                    matchTextDialog.hide();
                }
            });
            matchTextDialog.show();
        }
    }

    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo net = cm.getActiveNetworkInfo();
        if (net!= null && net.isAvailable() && net.isConnected()){
            return true;
        }   else {
            return false;
        }
    }


    public class viewAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        public viewAdapter(Context context){
            mInflater= LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return listaUsers.size();
        }

        @Override
        public Object getItem(int i) {
            return listaUsers.get(i);
        }

        @Override
        public long getItemId(int i) {
            return  i;
        }

        @Override
        public View getView(final int i, View view, ViewGroup viewGroup) {

            if(view==null){
                view=mInflater.inflate(R.layout.item,null);
            }
            final TextView txt_nombre= (TextView) view.findViewById(R.id.item_nombre);
            TextView txt_perfil= (TextView) view.findViewById(R.id.item_perfil);
            TextView txt_gen= (TextView) view.findViewById(R.id.item_gen);
            ImageView imagen= (ImageView) view.findViewById(R.id.item_img);


            txt_nombre.setText(listaUsers.get(i).getNombre());
            txt_perfil.setText(listaUsers.get(i).getPerfil());
            txt_gen.setText(listaUsers.get(i).getSexo());
            imagen.setImageURI(Uri.parse(listaUsers.get(i).getFoto()));
            return view;

        }
    }

}
