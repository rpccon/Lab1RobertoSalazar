package com.example.robertosalazar.robertosalazarlab;

import android.net.Uri;

/**
 * Created by Roberto Salazar on 4/4/2018.
 */
public class Usuario {

    String nombre="";
    String perfil="";
    String sexo="";
    String foto;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
}
